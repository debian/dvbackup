dvbackup for Debian
-------------------

  * dvbackup depends on rsbep, a useful error correction
    algorithm. On other architectures that i386 you have to use rsbepC, the
    somewhat slower version implemented in pure C, instead of rsbep.

  * Your system and the tool you use to generate the backup stream (tar for 
    example) should be able to provide 3.6 MB/sec _sustained_ datarate. 
    Otherwise you will get underruns and can't use the full capacity of 
    the tape.
  
  * Ideas of backup scripts are in /usr/share/doc/dvbackup/examples
 
  * A useful commandline for a backup could be (you have to start and stop
    recording yourself, these only do the streaming):

      # tar cpsvv /home /etc /usr /bin /sbin /root /var | \
      rsbep | \
      dvbackup --verbose --prefix 120 --set-backup-title "full 2004-02-19" | \
      dvconnect --send --underrun-data /usr/share/dvbackup/underrun-pal.dv \
       --buffers 512 --device /dev/video1394/0 -- - 

    For verification:
      
      # dvconnect --device /dev/video1394/0 | \
      dvbackup --decode --verbose | \
      rsbep -d -v | \
      tar Oxvv > /dev/null 

     And for rescue (never tested, as no one ever tests "worst case" ;-):

      # dvconnect --device /dev/video1394/0 | \
      dvbackup --decode --verbose | \
      rsbep -d -v | \
      tar xvv

 -- Robert Jordens <jordens@debian.org>, Thu Feb 19 20:33:29 CET 2004
 
