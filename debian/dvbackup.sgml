<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

  <!ENTITY dhfirstname "<firstname>Robert</firstname>">
  <!ENTITY dhsurname   "<surname>Jordens</surname>">
  <!ENTITY dhdate      "<date>Feb  19, 2004</date>">
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>jordens@debian.org</email>">
  <!ENTITY dhusername  "Robert Jordens">
  <!ENTITY dhucpackage "<refentrytitle>DVBACKUP</refentrytitle>">
  <!ENTITY dhpackage   "dvbackup">
  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2004</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Converter from arbitrary data to a DV stream
    </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
		<!--  -->
      <arg><option>--version</option></arg>
	  <arg><option>-n|--ntsc-mode</option></arg>
	  <arg><option>-d|--decode</option></arg>
	  <arg><option>-t|--verify</option></arg>
	  <arg><option>-b|--set-backup-title=TITLE</option></arg>
	  <arg><option>--set-picture=PPM-FILE</option></arg>
	  <arg><option>-v|--verbose</option></arg>
	  <arg><option>-p|--prefix=COUNT</option></arg>
	  <arg><option>--test=COUNT</option></arg>
	  <arg><option>-r|--recover</option></arg>
	  <arg><option>--enable-audio</option></arg>
	  <arg><option>-?|--help</option></arg>
	  <arg><option>--usage</option></arg>

    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> tool.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.</para>

   <para>
   As you probably know, current digital
   camcorders can save approximately 13 GB of data on those tiny DV
   cartridges at a speed of 3.6 MB/second. That's fast. Very fast. It's
   faster than most DAT streamers which only work at 1 MB/sec or less. We
   can not use all of the data, but 10 GB should be good enough for
   everyone.
   </para><para>
   That's nice, but how can we use this to save data on it? And here comes
   the fun part: If you read the DV documentation carefully, you will
   notice that the AC DCT coefficients of the video data blocks (8x8
   pixels in size) get a fixed amount of space in the DV data stream, but
   can be terminated earlier with a certain code sequence. So let's have
   some fun: We terminate the AC coefficients immediately leaving only the
   DC coefficient for a fancy penguin picture and use the rest for our
   backup data. Future implementations could easily add a little picture
   showing the currently written file or something like that.
   </para><para>
   Then there is the audio data, which is written uncompressed onto the
   tape. That means: We tell the camcorder at the beginning of each frame,
   that we won't use audio at all but fill the space reserved for it with
   data. Easy, but somewhat hacky. In fact, I don't know, if this works on
   every camcorder and not only on mine (a Sony VX700). Your mileage may
   vary.
   </para><para>
   To finally bring the data on tape, you have to use an additional
   utility, called dvconnect, which is (hopefully soon) included into
   libdv. Take a look at the patch manager if it's not in already. And
   then it's time to rock and roll:
   </para><para>
Advantages of dvbackup over other backup technologies</para>
<VariableList>
<VarListEntry><term></><ListItem><para>
      relatively cheap (the cheapest camcorder will be enough, but if you
       have already one...)
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      the tapes are quite cheap
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      open standard: if your streamer, aah camcorder dies you can rescue
       your data with any other one (except PAL/NTSC need to fit), you are
       not bound to a special company
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      it's faster than many streamers and it will be more comfortable -
       you can use the search-index function to "jump" to a recording
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      tapes (re)wind faster than many streamers
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      you do not need to rewind the tape to eject it
</para></LISTITEM></VarListEntry>
</VariableList>

<para>Disadvantages of dvbackup</para>
<VariableList><VarListEntry><term></><ListItem><para>
      you do not get any warranty :-)
</para></LISTITEM></VarListEntry></VariableList>

<para>Usage of the Unix client</para>
<VariableList><VarListEntry><term></><ListItem><para>
     Press record on your camcorder. (Or use your favorite avc control
       program for this. For the VX700 this doesn't work and you have to
       hack something together, that uses LANC. I might publish my
       "solution" for this soon...)
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
     Type "find . |cpio -o -H crc |dvbackup --prefix=125 |dvconnect -s"
       to stream directly to your camcorder. This most likely does only
       work on very fast harddisks and filesystems. You might try
       something like "find . |cpio -o -H crc |dvbackup --prefix=125
       |dvconnect -s -b 500" Alternatively, you can write the data in
       several parts on tape. Just go experimenting, and mail me the
       resulting backup scripts...
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      Stop your camcorder and rewind.
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      Now it's time to verify: Press play on tape ;-)
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
      Type "dvconnect |dvbackup -t" and watch for crc errors. The data
       corruption bug mentioned for version 0.0.1 seems to be fixed so
       there is no excuse in not using this little nifty program ;-)
</para></LISTITEM></VarListEntry><VarListEntry><term></><ListItem><para>
     If you want to restore: Do a simple "dvconnect |dvbackup -d|cpio
       -imV". CPIO will also happily tell you about CRC errors. So you
       might want to check using cpio's archive test mode too. But keep in
       mind, that cpio's CRC function is not that fast!
</para></LISTITEM></VarListEntry></VariableList>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>
	<para>This manual page was written by &dhusername; &dhemail; for the
	&debian; system (but may be used by others).  Permission is granted
	to copy, distribute and/or modify this document under the terms of
	the <acronym>GNU</acronym> General Public License, Version 2. On
	Debian systems, the full text of this license can be found in the
	file /usr/share/common-licenses/GPL-2.</para>
  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


