#Flags for fast code:
#CFLAGS=-Wall -O3 -mcpu=i686 -fomit-frame-pointer 
CFLAGS=-Wall -O3 -fomit-frame-pointer 

#Flags for profiling:
#CFLAGS=-Wall -O3 -pg

#install into:
install_dir=/usr/local/bin

all: dvbackup dvconnect Makefile

dvconnect: dvconnect.c Makefile
	gcc $(CFLAGS) -DHAVE_LIBPOPT -o dvconnect dvconnect.c -lpopt -lpthread

dvbackup: dvbackup.c minilogo.c Makefile
	gcc $(CFLAGS) -I/usr/include -o dvbackup dvbackup.c minilogo.c -lpopt -lm -lz

install: dvbackup dvconnect
	install dvbackup $(install_dir)
	install dvconnect $(install_dir)
	strip $(install_dir)/dvconnect
	strip $(install_dir)/dvbackup

clean:
	rm dvbackup
	rm dvconnect

